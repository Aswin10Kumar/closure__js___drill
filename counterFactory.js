
function counterFactory(){

    var count=0;
    return {
        increment: function(){
            return ++count;
        },
        decrement: function(){
            return --count;
        }
    }
}

module.exports=counterFactory;