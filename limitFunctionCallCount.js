function limitFunctionCallCount(cb, n) {
    let count=0;
   
    return function(...arg){
        if(count<n){
            count+=1;
            const cbReturn=cb(...arg);
            return cbReturn;
        }   
    }

}

module.exports=limitFunctionCallCount;