// function cacheFunction(cb) {
//     // Should return a function that invokes `cb`.
//     // A cache (object) should be kept in closure scope.
//     // The cache should keep track of all arguments have been used to invoke this function.
//     // If the returned function is invoked with arguments that it has already seen
//     // then it should return the cached result and not invoke `cb` again.
//     // `cb` should only ever be invoked once for a given set of arguments.
// }

const cache=require('../cacheFunction.js');
const callBack=(...arg)=>{
    return arg;
}
let n=5;
const CACHE_CALL=cache(callBack);
CACHE_CALL(5,2,3);
CACHE_CALL(10);
CACHE_CALL(1);
CACHE_CALL(1);
console.log(CACHE_CALL(5));