const callCount=require('../limitFunctionCallCount');
var count=0;
const callBack=(...arg)=>{
    return arg;
}
var n=3;
const callingReturn=callCount(callBack,n);
console.log(callingReturn(1,2,3,4,5));
console.log(callingReturn(2));
console.log(callingReturn(2,1));
console.log(callingReturn(2,1));
console.log(callingReturn(4,1));
callingReturn();