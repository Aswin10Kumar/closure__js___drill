function cache(callBack) {
    let CACHE = {};

    return function (...arg) {
        if (CACHE[arg] == undefined) {
            const val = callBack(...arg);
            CACHE[arg] = val;
        }
        return CACHE;
    };
}

module.exports = cache;

